"vundle
set rtp+=~/.vim/vundle.git/
call vundle#rc()
 
"Bundle 'rails.vim'
"括弧の中にカーソルがいるときのユーティリティ集
Bundle 'surround.vim' 
"履歴付クリップボード
Bundle 'YankRing.vim' 
"Bundle 'AutoClose'
"プロジェクトルートにchdir
Bundle 'teranex/vim-rooter'
"エラーチェッカー
Bundle 'scrooloose/syntastic' 
"カラースキーム
Bundle 'vim-scripts/Lucius' 
Bundle 'thinca/vim-quickrun' 
"言語リファレンスを見る
Bundle 'thinca/vim-ref' 
"Bundle 'motemen/git-vim'
"git
Bundle 'tpope/vim-fugitive' 
"Bundle 'vim-ruby/vim-ruby'
"git
Bundle 'Shougo/vimproc' 
Bundle 'Shougo/neocomplcache'
"Bundle 'Shougo/neocomplcache-snippets-complete'
Bundle 'https://github.com/Shougo/neosnippet'
Bundle 'Shougo/echodoc'
Bundle 'Shougo/unite.vim'
Bundle 'Shougo/vimfiler'
Bundle 'Shougo/vimshell'
"Bundle 'git://github.com/Shougo/clang_complete.git'
Bundle 'https://github.com/Shougo/neocomplcache-clang_complete.git'
Bundle 'https://github.com/Rip-Rip/clang_complete.git'
Bundle 'tsukkee/unite-help'
Bundle 'h1mesuke/unite-outline'
Bundle 'scrooloose/nerdtree'
Bundle 'mitechie/pyflakes-pathogen'
Bundle 'reinh/vim-makegreen'
Bundle 'lambdalisue/nose.vim'
Bundle 'sontek/rope-vim'
Bundle 'git://github.com/vim-scripts/pythoncomplete.git'
Bundle 'lambdalisue/vim-django-support'
Bundle 'msanders/cocoa.vim'
Bundle 'SonicTemplate.vim'
Bundle 'quickrun.vim'
Bundle 'sudo.vim'
Bundle 'slimv.vim'
Bundle 'kana/vim-smartchr'
Bundle 'Lokaltog/vim-powerline'
Bundle 'taku-o/vim-toggle'
Bundle 'Smooth-Scroll'
Bundle 'smartword'
Bundle 'matchit.zip'
Bundle 'grep.vim'
Bundle 'taglist.vim'
Bundle 'Source-Explorer-srcexpl.vim'
Bundle 'trinity.vim'
Bundle 'tyru/open-browser.vim'
Bundle 'rizzatti/funcoo.vim'
Bundle 'rizzatti/dash.vim'

syntax on
filetype on
filetype indent on
filetype plugin on
set hidden

"vi互換をやめる
set nocompatible

set backspace=indent,eol,start  "BSでなんでも消せるようにする
set formatoptions+=mM           "整形オプションにマルチバイト系を追加
set cursorline


"fundamental indent settings
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab

"show line number
set number
let lisp_rainbow=1

"賢いインデント
set autoindent
set smartindent
" 検索文字列が小文字の場合は大文字小文字を区別なく検索する(noignorecase)
set ignorecase
" インクリメンタルサーチ
set incsearch
"大文字と小文字があったらそれらを区別
set smartcase
" 全角スペースを視覚化
highlight ZenkakuSpace cterm=underline ctermfg=lightblue guibg=white
match ZenkakuSpace /　/
"OSのクリップボードを使う
set clipboard+=unnamed
"改行コードの自動認識
set fileformats=unix,dos,mac
set encoding=utf8
set fencs=utf8,ujis,sjis
"emacs like
map! <c-a> <home>
map! <c-e> <end>
map! ¥ \
nnoremap <c-a> <home>
nnoremap <c-e> <end>
nnoremap ¥ \
"Esc->Escで検索結果をクリア
nnoremap <silent><ESC><ESC> :nohlsearch<CR><ESC>
"Visualモード時にvで行末まで選択する
vnoremap v $h



"for NERD tree
nmap <Leader>n :NERDTreeToggle<CR>

" Automatic `:!chmod +x %`. {{{
command! -bar AutoChmodDisable let b:disable_auto_chmod = 1
command! -bar AutoChmodEnable  unlet! b:disable_auto_chmod
autocmd BufWritePost * call s:auto_chmod()
function! s:check_auto_chmod() "{{{
    return !exists('b:disable_auto_chmod')
    \   && getfperm(expand('%'))[2] !=# 'x'
    \   && getline(1) =~# '^#!'
    \   && executable('chmod')
endfunction "}}}
function! s:auto_chmod()
    if s:check_auto_chmod()
        !chmod +x %
    endif
endfunction
" }}}

" Automatic mkdir when :edit nonexistent-file {{{
" http://vim-users.jp/2011/02/hack202/
augroup vimrc-auto-mkdir
    autocmd!
    autocmd BufWritePre * call s:auto_mkdir(expand('<afile>:p:h'), v:cmdbang)
    function! s:auto_mkdir(dir, force)
        if !isdirectory(a:dir)
        \   && (a:force
        \       || input("'" . a:dir . "' does not exist. Create? [y/N]") =~? '^y\%[es]$')
            call mkdir(iconv(a:dir, &encoding, &termencoding), 'p')
        endif
    endfunction
augroup END " }}}

" Paste Mode
let paste_mode = 0 " 0 = normal, 1 = paste
 
function! Paste_on_off()
    if g:paste_mode == 0
        set paste
        let g:paste_mode = 1
    else
        set nopaste
        let g:paste_mode = 0
    endif
    return
endfunc
" Paste Mode <F10>
nnoremap <silent> <F10> :call Paste_on_off()<CR>
set pastetoggle=<F10>

" 補完ウィンドウの設定
set completeopt=menuone
 
" 起動時に有効化
let g:neocomplcache_enable_at_startup = 1
 
" 大文字が入力されるまで大文字小文字の区別を無視する
let g:neocomplcache_enable_smart_case = 1
 
" _(アンダースコア)区切りの補完を有効化
let g:neocomplcache_enable_underbar_completion = 1
 
let g:neocomplcache_enable_camel_case_completion  =  1
 
" ポップアップメニューで表示される候補の数
let g:neocomplcache_max_list = 20
 
" シンタックスをキャッシュするときの最小文字長
let g:neocomplcache_min_syntax_length = 3
 
" ディクショナリ定義
let g:neocomplcache_dictionary_filetype_lists = {
    \ 'default' : '',
    \ 'vimshell' : $HOME.'/.vimshell_hist'
    \ }
 
if !exists('g:neocomplcache_keyword_patterns')
        let g:neocomplcache_keyword_patterns = {}
endif
let g:neocomplcache_keyword_patterns['default'] = '\h\w*'

"スニペット展開候補があれば展開を，そうでなければbash風補完を．
imap <expr><Tab> neocomplcache#sources#snippets_complete#expandable() ? "\<Plug>(neocomplcache_snippets_expand)" : (pumvisible() ? (neocomplcache#sources#snippets_complete#expandable() ? "\<Plug>(neocomplcache_snippets_expand)" : neocomplcache#complete_common_string()):"\<Tab>")

" スニペットを展開する。スニペットが関係しないところでは行末まで削除
"inoremap <expr><Tab> neocomplcache#sources#snippets_complete#expandable() ? "\<Plug>(neocomplcache_snippets_expand)" : "\<Tab>"
 
" 前回行われた補完をキャンセルします
inoremap <expr><C-g> neocomplcache#undo_completion()
 
" 補完候補のなかから、共通する部分を補完します
inoremap <expr><C-l> neocomplcache#complete_common_string()
 
" 補完候補が表示されている場合は確定。そうでない場合は改行
inoremap <expr><CR>  pumvisible() ? neocomplcache#close_popup() : "<CR>"
 
"tabで補完候補の選択を行う
inoremap <expr><S-TAB> pumvisible() ? "\<Up>" : "\<S-TAB>"
 
" <C-h>や<BS>を押したときに確実にポップアップを削除します
inoremap <expr><C-h> neocomplcache#smart_close_popup().”\<C-h>”
 
"inoremap <expr><Tab> pumvisible() ? neocomplcache#close_popup() : "\<Tab>"
 
" 現在選択している候補をキャンセルし、ポップアップを閉じます
"inoremap <expr><C-e> neocomplcache#cancel_popup()

" Enable omni completion.
autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags
autocmd FileType c setlocal omnifunc=ccomplete#Complete

" add neocomplcache option
let g:neocomplcache_force_overwrite_completefunc=1

" For clang_complete.
let g:neocomplcache_force_overwrite_completefunc = 1
"let g:neocomplcache_force_omni_patterns.c =
"      \ '[^.[:digit:] *\t]\%(\.\|->\)'
"let g:neocomplcache_force_omni_patterns.cpp =
"      \ '[^.[:digit:] *\t]\%(\.\|->\)\|\h\w*::'
let g:clang_complete_auto = 0
let g:clang_auto_select = 0
let g:clang_use_library   = 1

set mouse=a
set helplang& helplang=ja,en

let g:home = getcwd()
let t:cwd = getcwd()

