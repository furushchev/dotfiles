;;; package --- Summary: init.el
;;; Commentary:
;; -*- coding: utf-8 -*-

;;; Code:

;; PATH
(add-to-list 'load-path "~/.emacs.d/elisp")
(require 'cl-lib)
(setq user-full-name "Yuki Furuta")
(setq user-mail-address "furushchev@jsk.imi.i.u-tokyo.ac.jp")

;;patterns for determining version and environment
(defvar oldemacs-p (< emacs-major-version 22))  ; 22 未満
(defvar emacs22-p (<= emacs-major-version 22))  ; 22 以下
(defvar emacs23-p (>= emacs-major-version 23))  ; 23 以上
(defvar emacs24-p (>= emacs-major-version 24))  ; 24 以上

(defun x->bool (elt) (not (not elt)))
(defvar darwin-p  (eq system-type 'darwin))
(defvar ns-p      (eq window-system 'ns))
(defvar carbon-p  (eq window-system 'mac))
(defvar linux-p   (eq system-type 'gnu/linux))
(defvar cygwin-p  (eq system-type 'cygwin))
(defvar nt-p      (eq system-type 'windows-nt))
(defvar meadow-p  (featurep 'meadow))
(defvar windows-p (or cygwin-p nt-p meadow-p))

(defvar rosdistro (getenv "ROS_DISTRO"))

;; yes/no -> y/n
(fset 'yes-or-no-p 'y-or-n-p)

;; assign goto-line to M-g
(global-set-key "\M-g" 'goto-line)
(global-set-key "\C-h" 'backward-delete-char)

;; sync killring and clipboard on OSX
(when darwin-p
  (defun copy-from-osx ()
	(shell-command-to-string "pbpaste"))
  (defun paste-to-osx (text &optional push)
	(let ((process-connection-type nil))
	  (let ((proc (start-process "pbcopy" "*Messages*" "pbcopy")))
		(process-send-string proc text)
		(process-send-eof proc))))
  (setq interprogram-cut-function 'paste-to-osx)
  (setq interprogram-paste-function 'copy-from-osx))

(defmacro submodule (n &rest body)
  """Check if lobrary exists, then require it."""
  `(when (locate-library (symbol-name ,n))
    (require ,n)
    ,@body))

;;load init files
(if darwin-p
    (require 'cask)
    (require 'cask "~/.cask/cask.el"))
(cask-initialize)

(require 'init-auto-insert)
(require 'init-formatting) ;encode,font,format...
(if window-system (require 'init-window)) ;window-mode
;;(require 'init-auto-install) ;auto-install,install-elisp
;;(require 'init-gtags) ;関数移動gtags
(require 'init-auto-complete) ;auto-complete関連
(require 'init-major-mode) ;各種major-modeカスタマイズ
(require 'init-helm)
(require 'init-utils) ;anythingなど
(put 'upcase-region 'disabled nil)

;; launch emacs server
(require 'server)
(unless (server-running-p)
  (server-start))

(provide 'init)
;;; init.el ends here
