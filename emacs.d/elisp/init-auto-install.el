;; http://d.hatena.ne.jp/rubikitch/20070726/1186048104
(when (locate-library "install-elisp")
  (require 'install-elisp)
  (setq install-elisp-repository-directory "~/.emacs.d/"))

;; (install-elisp-from-emacswiki "auto-install.el")
;; Emacs テクニックバイブルより
;; Emacs インストーラー, install-elisp の後継
(when (locate-library "auto-install")
  (require 'auto-install)
  (add-to-list 'load-path auto-install-directory)
  (auto-install-update-emacswiki-package-name nil)
  (auto-install-compatibility-setup)
  (setq ediff-window-setup-function 'ediff-setup-windows-plain))

;;; This was installed by package-install.el.
;;; This provides support for the package system and
;;; interfacing with ELPA, the package archive.
;;; Move this code earlier if you want to reference
;;; packages in your .emacs.
;; (install-elisp "http://tromey.com/elpa/package-install.el")
;; Emacs テクニックバイブルより
;; Emacs パッケージマネージャー, auto-install と補完しあうもの
(when emacs24-p
  (when
      (load
       (expand-file-name "~/.emacs.d/elpa/package.el"))
    (if (locate-library "melpa")
        (require 'melpa)
;	  (progn
;		(add-to-list 'package-archives '("melpa" . "http://melpa.milkbox.net/packages/") t)
;		(add-to-list 'package-archives '("marmalade" . "http://marmalade-repo.org/packages/"))
;		(package-initialize)
;		)
      )))

(when emacs23-p
  (load (expand-file-name "~/.emacs.d/elpa/package23.el"))
  (require 'package)
  ;; Any add to list for package-archives (to add marmalade or melpa) goes here
  (add-to-list 'package-archives 
	       '("marmalade" . "http://marmalade-repo.org/packages/"))
  (add-to-list 'package-archives
	       '("melpa" . "http://melpa.milkbox.net/packages/"))
  (package-initialize))

(require 'cl-lib)

(provide 'init-auto-install)
