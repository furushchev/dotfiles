  ;;関数移動gtags
(when (locate-library "gtags")
    (require 'gtags)
    (global-set-key "\M-t" 'gtags-find-tag)  ; 関数定義元へ
    (global-set-key "\M-r" 'gtags-find-rtag) ; 関数の参照元へ
    (global-set-key "\M-s" 'gtags-find-symbol) ; 変数へ(うまくうごかない...)
    (global-set-key "\M-p" 'gtags-find-pattern) ; マッチした行へ
    (global-set-key "\M-f" 'gtags-find-file)    ; マッチしたファイル名へ
    (global-set-key [?\C-,] 'gtags-pop-stack)   ; ジャンプ前の場所へ
    (add-hook 'c-mode-common-hook '(lambda () (gtags-mode 1)))
    (setq gtags-mode-hook '(lambda () (setq gtags-path-style 'relative)))
    ;; http://d.hatena.ne.jp/rubikitch/20080304/gtagscurrentline
    (defun gtags-current-line ()
      (save-excursion
        (save-restriction
          (widen)
          (forward-char 1)
          (count-lines (point-min) (point)))))
    (defadvice gtags-goto-tag (around show-current-position activate)
      "Point to the entry of current file/line."
      (let ((bfn buffer-file-name)
            (curline (gtags-current-line)))
        ad-do-it
        (when (and bfn (eq major-mode 'gtags-select-mode))
          (let ((path (if (looking-at "[^ \t]+[ \t]+[0-9]+[ \t]/[^ \t]+[ \t]")
                          bfn
                        (file-relative-name bfn))))
            (and (or (re-search-forward (format "%d[ \t]+%s" curline (regexp-quote path)) nil t)
                     (search-forward path nil t))
                 (beginning-of-line)))))))

(provide 'init-gtags)
