;;font setting
(when (and (or emacs23-p emacs24-p) linux-p)
  (add-to-list 'default-frame-alist '(font . "ricty-16"))
  )

(when (and (or emacs23-p emacs24-p) darwin-p)
  (set-face-attribute 'default nil
                      :family "ricty"
                      :height 170)
  (set-fontset-font
   (frame-parameter nil 'font)
   'japanese-jisx0208
   '("Hiragino Maru Gothic Pro" . "iso10646-1"))
  (set-fontset-font
   (frame-parameter nil 'font)
   'katakana-jisx0201
   '("Hiragino Maru Gothic Pro" . "iso10646-1"))
  ;; (set-fontset-font
  ;;  (frame-parameter nil 'font)
  ;;  'mule-unicode-0100-24ff
  ;;  '("monaco" . "iso10646-1"))
  (setq face-font-rescale-alist
        '(("^-apple-hiragino.*" . 1.2)
          (".*osaka-bold.*" . 1.2)
          (".*osaka-medium.*" . 1.2)
          (".*courier-bold-.*-mac-roman" . 1.0)
          (".*monaco cy-bold-.*-mac-cyrillic" . 0.9)
          (".*monaco-bold-.*-mac-roman" . 0.9)
          ("-cdac$" . 1.3))))

;; color-theme + color-theme-solarized
(add-to-list 'load-path "~/.emacs.d/color-theme")
(add-to-list 'load-path "~/.emacs.d/color-theme/emacs-color-theme-solarized")
(submodule 'color-theme
  (color-theme-initialize)
  (when (require 'color-theme-solarized)
    (color-theme-solarized-dark)))

;;現在行に色をつける
(global-hl-line-mode t)

;;選択範囲に色をつける
(setq transient-mark-mode t)

;;カーソル位置にペースト
(setq mouse-yank-at-point t)

;;ログ行数を10000に
(setq message-log-max 10000)

;;タイトルバーにファイル名表示
(setq frame-title-format (format "emacs@%s : %%f" (system-name)))

;;時刻表示(ついでにメールも)
(setq display-time-string-forms
                 '(month "/" day " " dayname " " 
                   24-hours ":" minutes " "
                   (if mail " Mail" "") ))
(display-time)
;(display-battery-mode)


;;スタートアップ非表示
(setq inhibit-startup-message t)

(when emacs24-p
  (add-to-list 'default-frame-alist '(alpha . 95)) ;デフォルトの透明度
  (set-frame-parameter nil 'alpha 95) ;カレントウインドウの透明度
)

;;ウインドウサイズ
(set-frame-width (next-frame) 100)
(set-frame-height (next-frame) 60)

;;time-stamp
(require 'time-stamp)
(add-hook 'before-save-hook 'time-stamp)
(setq time-stamp-toggle-active t)
(setq time-stamp-start "last updated: ")
(setq time-stamp-format "%02d.%02m.%04y %02H:%02M:%02S %u")
(setq time-stamp-end " ¥¥|$")
(setq compilation-window-height 5)

;; hide-menu-bar
(tool-bar-mode -1)

;; disable accel mouse scroll
(setq mouse-wheel-progressive-speed nil)

(provide 'init-window)
