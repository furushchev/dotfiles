(submodule 'helm-config
  (helm-mode 1)

  (define-key global-map (kbd "M-x")     'helm-M-x)
  (define-key global-map (kbd "C-x C-f") 'helm-find-files)
  (define-key global-map (kbd "C-x C-r") 'helm-recentf)
  (define-key global-map (kbd "M-y")     'helm-show-kill-ring)
  (define-key global-map (kbd "C-c i")   'helm-imenu)
  (define-key global-map (kbd "C-x b")   'helm-buffers-list)

  (define-key helm-map (kbd "C-h") 'delete-backward-char)
  (define-key helm-find-files-map (kbd "C-h") 'delete-backward-char)
  (define-key helm-find-files-map (kbd "TAB") 'helm-execute-persistent-action)
  (define-key helm-read-file-map (kbd "TAB") 'helm-execute-persistent-action)

  ;; Disable helm in some functions
  (add-to-list 'helm-completing-read-handlers-alist '(find-alternate-file . nil))

  ;; Emulate `kill-line' in helm minibuffer
  (setq helm-delete-minibuffer-contents-from-point t)
  (defadvice helm-delete-minibuffer-contents (before helm-emulate-kill-line activate)
    "Emulate `kill-line' in helm minibuffer"
    (kill-new (buffer-substring (point) (field-end))))

  (defadvice helm-ff-kill-or-find-buffer-fname (around execute-only-if-exist activate)
    "Execute command only if CANDIDATE exists"
    (when (file-exists-p candidate)
      ad-do-it))

  (defadvice helm-ff-transform-fname-for-completion (around my-transform activate)
    "Transform the pattern to reflect my intention"
    (let* ((pattern (ad-get-arg 0))
           (input-pattern (file-name-nondirectory pattern))
           (dirname (file-name-directory pattern)))
      (setq input-pattern (replace-regexp-in-string "\\." "\\\\." input-pattern))
      (setq ad-return-value
            (concat dirname
                    (if (string-match "^\\^" input-pattern)
                        ;; '^' is a pattern for basename
                        ;; and not required because the directory name is prepended
                        (substring input-pattern 1)
                      (concat ".*" input-pattern))))))

  ;; submodule
  (submodule 'helm-swoop
   (global-set-key (kbd "M-i") 'helm-swoop)
   (global-set-key (kbd "M-I") 'helm-swoop-back-to-last-point)
   (global-set-key (kbd "C-c M-i") 'helm-multi-swoop)
   (global-set-key (kbd "C-x M-i") 'helm-multi-swoop-all)

   ;; When doing isearch, hand the word over to helm-swoop
   (define-key isearch-mode-map (kbd "M-i") 'helm-swoop-from-isearch)
   ;; From helm-swoop to helm-multi-swoop-all
   (define-key helm-swoop-map (kbd "M-i") 'helm-multi-swoop-all-from-helm-swoop)
   ;; When doing evil-search, hand the word over to helm-swoop
   ;; (define-key evil-motion-state-map (kbd "M-i") 'helm-swoop-from-evil-search)

   ;; Save buffer when helm-multi-swoop-edit complete
   (setq helm-multi-swoop-edit-save t)

   ;; If this value is t, split window inside the current window
   (setq helm-swoop-split-with-multiple-windows nil)

   ;; Split direcion. 'split-window-vertically or 'split-window-horizontally
   (setq helm-swoop-split-direction 'split-window-vertically)

   ;; If nil, you can slightly boost invoke speed in exchange for text color
   (setq helm-swoop-speed-or-color nil))

  (submodule 'helm-descbinds
   (if emacs24-p
       (helm-descbinds-mode)
     (helm-descbinds-mode 1)))

  (submodule 'helm-ag-r
   (setq
    helm-ag-r-google-contacts-user "furushchev@gmail.com"
    helm-ag-r-google-contacts-lang "ja_JP.UTF-8"
    helm-ag-r-option-list '("-S -U --hidden"
                            "-S -U -g")
    helm-ag-r-requires-pattern 3   
    helm-ag-r-input-idle-delay 0.5 
    helm-ag-r-use-no-highlight t   
    helm-ag-r-candidate-limit 1000))

  (submodule 'helm-open-github
   (global-set-key (kbd "C-c o f") 'helm-open-github-from-file)
   (global-set-key (kbd "C-c o c") 'helm-open-github-from-commit)
   (global-set-key (kbd "C-c o i") 'helm-open-github-from-issues)
   (global-set-key (kbd "C-c o p") 'helm-open-github-from-pull-requests))

  (submodule 'helm-git-project
   (define-key global-map (kbd "C-;") 'helm-git-project))

  (submodule 'helm-recentf
   (global-set-key (kbd "C-x C-r") 'helm-recentf))
  
  ) ;; submodule 'helm



(provide 'init-helm)

