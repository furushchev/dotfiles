;; projectile
(submodule 'projectile
  (projectile-global-mode))

;; powerline
(submodule 'powerline
  (powerline-default-theme)
  (set-face-attribute 'mode-line nil
                      :foreground "#171717"
                      :background "#fff"
                      :box nil)
  (set-face-attribute 'powerline-active1 nil
                      :foreground "#00ff00"
                      :background "#fff"
                      :inherit 'mode-line)
  (set-face-attribute 'powerline-active2 nil
                      :foreground "#0000ff"
                      :background "#fff"
                      :inherit 'mode-line)
  (set-face-attribute 'mode-line-inactive nil
                      :foreground "#171717"
                      :background "#fffacd"))

;;buffer move util
(submodule 'buffer-move
  (global-set-key (kbd "<C-S-up>")     'buf-move-up)
  (global-set-key (kbd "<C-S-down>")   'buf-move-down)
  (global-set-key (kbd "<C-S-left>")   'buf-move-left)
  (global-set-key (kbd "<C-S-right>")  'buf-move-right))

;;yasnippet
(submodule 'yasnippet
  (add-to-list 'yas-snippet-dirs "~/.emacs.d/snippets")
;;  (setq yas-snippet-dirs "~/.emacs.d/elpa/yasnippet-20130504.336/snippets")
  (yas-global-mode 1)
  (custom-set-variables '(yas-trigger-key "TAB"))
  (setf yas/indent-line nil)
  (setf (symbol-function 'yas-active-keys)
        (lambda ()
          (remove-duplicates (mapcan #'yas--table-all-keys (yas--get-snippet-tables))))))

;;quickrun (To run M-x quickrun)
(submodule 'quickrun)

;;duplicate C-u
;; (submodule 'duplicate-thing
;;   (global-set-key (kbd "M-c") 'duplicate-thing))

;;rosemacs
(when rosdistro
  (add-to-list 'load-path (format "/opt/ros/%s/share/emacs/site-lisp" rosdistro))
  (if (locate-library "rosemacs-config")
      (require 'rosemacs-config)
    (progn
      (add-to-list 'load-path "~/.emacs.d/elisp/rosemacs")
      (when (locate-library "rosemacs")
        (require 'rosemacs)
        (invoke-rosemacs)
        (global-set-key "\C-x\C-r" ros-keymap)))))

;; dash-at-point only for OSX
(when darwin-p
  (when (locate-library "dash-at-point")
	(autoload 'dash-at-point "dash-at-point"
	  "Search the word at point with Dash." t nil)
	(global-set-key "\C-cd" 'dash-at-point)
	)
  )

;; ipython
(submodule 'ipython
  (setq ipython-command "/usr/local/bin/ipython"))

;; anzu
(submodule 'anzu
  (global-anzu-mode +1)
  (custom-set-variables
   '(anzu-mode-lighter "")
   '(anzu-deactivate-region t)
   '(anzu-search-threshold 1000))
  )

;; popwin
(submodule 'popwin
  (setq display-buffer-function 'popwin:display-buffer))

;; goto-chg
(submodule 'goto-chg
  (global-set-key "\C-xj" 'goto-last-change)
  (global-set-key "\C-xJ" 'goto-last-change-reverse))

;; exec-path-from-shell
(let ((envs '("PATH" "GOROOT" "GOPATH" "PYTHONPATH" "ROS_PACKAGE_PATH")))
  (exec-path-from-shell-copy-envs envs))

;; git
(submodule 'magit
 (setq magit-last-seen-setup-instructions "1.4.0"))
(submodule 'git-gutter+
 (global-git-gutter+-mode t))

;; dired
(submodule 'dired-k
 (define-key dired-mode-map (kbd "K") 'dired-k)
 (define-key dired-mode-map (kbd "g") 'dired-k)
 (add-hook 'dired-initial-position-hook 'dired-k))

;; direx
(submodule 'direx
 (global-set-key (kbd "C-x C-j") 'direx:jump-to-directory)
 (push '(direx:direx-mode :position left :width 25 :dedicated t)
       popwin:special-display-config)
 (global-set-key (kbd "C-x C-j") 'direx:jump-to-directory-other-window)
 (require 'direx-k)
 (global-set-key (kbd "C-\\") 'direx-project:jump-to-project-root-other-window)
 (define-key direx:direx-mode-map (kbd "K") 'direx-k))

;; flycheck
(add-hook 'after-init-hook #'global-flycheck-mode)
(eval-after-load 'flycheck
  '(custom-set-variables
    '(flycheck-display-errors-function #'flycheck-pos-tip-error-messages)))

;; import-popwin
(submodule 'import-popwin)

;; multi-term
(submodule 'multi-term
  (setq multi-term-program shell-file-name)
  (add-to-list 'term-unbind-key-list '"M-x")
  (add-hook 'term-mode-hook
            '(lambda ()
               (define-key term-raw-map (kbd "C-y") 'term-paste)))
  (global-set-key (kbd "C-c t") '(lambda ()
                                   (interactive)
                                   (if (get-buffer "*terminal<1>*")
                                       (switch-to-buffer "*terminal<1>*")
                                     (multi-term))))
  (global-set-key (kbd "C-c n") 'multi-term-next)
  (global-set-key (kbd "C-c p") 'multi-term-prev)
)

;; change the visual of same file on the buffer
(submodule 'uniquify
 (setq uniquify-buffer-name-style 'post-forward-angle-brackets))

;; replace on grep
(submodule 'wgrep
 (setq wgrep-enable-key "r"))

;; add rename feature for dired
(submodule 'wdired
  (define-key dired-mode-map "r" 'wdired-change-to-wdired-mode))


(provide 'init-utils)
