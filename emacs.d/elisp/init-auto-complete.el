(submodule 'auto-complete
  (require 'auto-complete-config)
  (global-auto-complete-mode t)
  (setq ac-auto-start 2)
  (setq ac-delay 0.05)
  (setq ac-use-comphist t)
  (setq ac-auto-show-menu 0.05)
  (setq ac-quick-help-delay 0.5)
  (setq ac-ignore-case nil)
  (setq ac-dwim t) ; To get pop-ups with docs even if a word is uniquely completed
  (define-key ac-completing-map (kbd "C-n") 'ac-next)
  (define-key ac-completing-map (kbd "C-p") 'ac-previous)

  ;; load submodules
  (submodule 'popup)

  (submodule 'fuzzy
   (setq ac-use-fuzzy t))

  ;; (submodule 'ac-ispell
  ;;  (custom-set-variables '(ac-ispell-requires 4))

  ;;  (eval-after-load "auto-complete"
  ;;    '(progn
  ;;       (ac-ispell-setup)))

  ;;  (add-hook 'git-commit-mode-hook 'ac-ispell-ac-setup)
  ;;  (add-hook 'mail-mode-hook 'ac-ispell-ac-setup)
  ;;  (add-hook 'text-mode-hook 'ac-ispell-ac-setup))

  ;;----------------------------------------------------------------------------
  ;; Use Emacs' built-in TAB completion hooks to trigger AC (Emacs >= 23.2)
  ;;----------------------------------------------------------------------------
  (setq tab-always-indent 'complete)  ;; use 't when auto-complete is disabled
  (add-to-list 'completion-styles 'initials t)

  ;; hook AC into completion-at-point
  (defun set-auto-complete-as-completion-at-point-function ()
    (setq completion-at-point-functions '(auto-complete)))
  (add-hook 'auto-complete-mode-hook 'set-auto-complete-as-completion-at-point-function)
  (set-default 'ac-sources
               '(ac-source-dictionary
                 ac-source-words-in-buffer
                 ac-source-words-in-same-mode-buffers
                 ac-source-words-in-all-buffer))

  (dolist (mode '(magit-log-edit-mode log-edit-mode org-mode text-mode haml-mode
                                      sass-mode yaml-mode csv-mode espresso-mode haskell-mode
                                      html-mode nxml-mode sh-mode smarty-mode clojure-mode
                                      lisp-mode textile-mode markdown-mode tuareg-mode
                                      js3-mode css-mode less-css-mode))
    (add-to-list 'ac-modes mode))


  ;; Exclude very large buffers from dabbrev
  (defun sanityinc/dabbrev-friend-buffer (other-buffer)
    (< (buffer-size other-buffer) (* 1 1024 1024)))

  (setq dabbrev-friend-buffer-function 'sanityinc/dabbrev-friend-buffer))

;;bash completion
(submodule 'bash-completion
  (bash-completion-setup))

;;; for python
(submodule 'jedi
;; (setenv "PYTHONPATH" "/usr/local/lib/python2.7/site-packages")
 (add-hook 'python-mode-hook 'jedi:setup)
 (setq-default jedi:complete-on-dot t))

(provide 'init-auto-complete)
