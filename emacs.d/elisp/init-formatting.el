;;エンコード
(set-language-environment 'Japanese)
(set-default-coding-systems 'utf-8-unix)
(set-terminal-coding-system 'utf-8-unix)
(set-keyboard-coding-system 'utf-8-unix)
(set-clipboard-coding-system 'sjis-mac)
(set-buffer-file-coding-system 'utf-8-unix)
(when (not file-name-coding-system)
  (set-file-name-coding-system 'utf-8))
(global-font-lock-mode t)
(prefer-coding-system 'utf-8)

;;ibus
;; (when (and linux-p (locate-library "ibus"))
;;   (require 'ibus)
;;   (add-hook 'after-init-hook 'ibus-mode-on)

;;   ;; use C-SPC for Set Mark
;;   (ibus-define-common-key ?\C-\s nil)

;;   ;; use C-/ for Undo
;;   (ibus-define-common-key ?\C-/ nil)

;;   ;; change cursor color along with ibus-status
;;   (setq ibus-cursor-color '("firebrick" "dark orange" "royal blue"))

;;   ;; share input-state with all buffers
;;   (setq ibus-mode-local nil)

;;   (setq ibus-prediction-window-position t)
;;   (ibus-disable-isearch)
;;   (add-hook 'minibuffer-setup-hook 'ibus-disable)
;;   (define-key global-map [zenkaku-hankaku] 'ibus-toggle)
;;   )

;;indent
(setq default-tab-width 2)
(setq tab-width 2)
(setq-default indent-tabs-mode nil)
(setq js-indent-level 2)
(setq c-basic-offset 2)

;; add color space,tab,zenkaku-space
(defface my-face-b-1 '((t (:background "gray"))) nil)
(defface my-face-b-2 '((t (:background "red"))) nil)
(defface my-face-u-1 '((t (:background "red"))) nil)
(defvar my-face-b-1 'my-face-b-1)
(defvar my-face-b-2 'my-face-b-2)
(defvar my-face-u-1 'my-face-u-1)
(defadvice font-lock-mode (before my-font-lock-mode ())
  (font-lock-add-keywords
   major-mode
   '(
     ("\t" 0 my-face-b-1 append)
     ("　" 0 my-face-b-2 append)
     ("[ \t]+$" 0 my-face-u-1 append)
     )))
(ad-enable-advice 'font-lock-mode 'before 'my-font-lock-mode)
(ad-activate 'font-lock-mode)


;;comment macro
(global-set-key "\C-cc" 'comment-region)    ; C-c c を範囲指定コメントに
(global-set-key "\C-cu" 'uncomment-region)  ; C-c u を範囲指定コメント解除に

;;括弧
(show-paren-mode t)

;;カーソル位置表示
(column-number-mode t)
(line-number-mode t)

;;スプラッシュスクリーンを非表示
(setq inhibit-startup-screen t)

;;Cocoa Emacsでバックスラッシュが入力できない問題対策
(when darwin-p
  (define-key global-map [?\¥] [?\\])
  (define-key global-map [?\C-¥] [?\C-\\])
  (define-key global-map [?\M-¥] [?\M-\\])
  (define-key global-map [?\C-\M-¥] [?\C-\M-\\]))

;; emacs-mozc
(submodule
 'mozc
 (setq default-input-method "japanese-mozc")
 )

;; comment out
(global-set-key (kbd "C-M-;") 'rough-comment)
(defun rough-comment (bg ed)
  "ざっくりとコメントアウト（解除）します。"
  (interactive (list (point) (mark)))
  (if (not mark-active)
      (save-excursion
        (comment-or-uncomment-region (progn (beginning-of-line) (point))
                                     (progn (end-of-line) (point))))
    (save-excursion
      (comment-or-uncomment-region
       (progn (goto-char (if (< bg ed) bg ed)) (beginning-of-line) (point))
       (progn (goto-char (if (< bg ed) ed bg)) (end-of-line) (point))))))

;; ;; ;; C-qで移動
;; (defun match-paren (arg)
;;   "Go to the matching parenthesis if on parenthesis."
;;   (interactive "p")
;;   (cond ((looking-at "\\s\(") (forward-list 1) (backward-char 1))
;;         ((looking-at "\\s\)") (forward-char 1) (backward-list 1))
;;         )
;;   )
;; (global-set-key "\C-Q" 'match-paren)

(submodule 'expand-region
 (global-set-key (kbd "C-,") 'er/expand-region)
 (global-set-key (kbd "C-<") 'er/contract-region)
 (transient-mark-mode t))

(submodule 'wrap-region
 (wrap-region-global-mode t)
 (wrap-region-add-wrapper "/*" "*/" "#" '(c++-mode css-mode java-mode javascript-mode))
 (wrap-region-add-wrapper "#|" "|#" "#" '(euslisp-mode common-lisp-mode))
)

;; (submodule 'undo-tree
;;   (global-undo-tree-mode t)
;;   (global-set-key (kbd "M-/") 'undo-tree-redo))

(provide 'init-formatting)
