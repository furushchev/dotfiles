;; C/C++ ---------------------------------------------------------
(add-hook 'c-mode-common-hook
          '(lambda ()                    ; style
             (c-set-style "linux")))
(setq auto-mode-alist
      (cons '("\\.h$" . c++-mode) auto-mode-alist))

(defun c++-mode-hook-indent ()
  (setq c-basic-offset 2)
  (c-set-offset 'substatement-open 0))
(add-hook 'c++-mode-hook 'c++-mode-hook-indent)

;; from http://www.emacswiki.org/emacs/CPlusPlusMode
;; 標準 C++ ライブラリを font-lock する設定
(require 'cl)
(defun file-in-directory-list-p (file dirlist)
  "Returns true if the file specified is contained within one of
  the directories in the list. The directories must also exist."
  (let ((dirs (mapcar 'expand-file-name dirlist))
        (filedir (expand-file-name (file-name-directory file))))
    (and
     (file-directory-p filedir)
     (member-if (lambda (x)           ; Check directory prefix matches
                  (string-match (substring x 0 (min(length filedir) (length x))) filedir))
                dirs))))
(defun buffer-standard-include-p ()
  "Returns true if the current buffer is contained within one of
  the directories in the INCLUDE environment variable."
  (and (getenv "INCLUDE")
       (file-in-directory-list-p buffer-file-name (split-string (getenv "INCLUDE") path-separator))))
(add-to-list 'magic-fallback-mode-alist '(buffer-standard-include-p . c++-mode))

;; ruby ----------------------------------------------------------
(submodule 'ruby-mode
  (setq auto-mode-alist
        (cons '("\\.rb$" . ruby-mode) auto-mode-alist)))

;; css-mode ------------------------------------------------------


;; lua -----------------------------------------------------------
(when (locate-library "lua-mode")
  (setq auto-mode-alist (cons '("\\.lua$" . lua-mode) auto-mode-alist))
  (autoload 'lua-mode "lua-mode" "Lua editing mode." t))

;; doxymacs ------------------------------------------------------
(when (locate-library "doxymacs")
  (require 'doxymacs)
  (add-hook 'c-mode-common-hook 'doxymacs-mode)
  (defun my-doxymacs-font-lock-hook ()
    (if (or (eq major-mode 'c-mode) (eq major-mode 'c++-mode))
        (doxymacs-font-lock)))
  (add-hook 'font-lock-mode-hook 'my-doxymacs-font-lock-hook)
  )

;; PHP ------------------------------------------------------
(when (locate-library "php-mode")
  (require 'php-mode)
  (add-to-list 'auto-mode-alist '("\\.php$" . php-mode))
  (setq php-mode-force-pear t)
  (add-hook 'php-mode-hook
            '(lambda ()
               (setq indent-tabs-mode t)
               (setq default-tab-width 4)
               (setq tab-width 4)
               (setq c-basic-offset 4)
               (setq tab-stop-list '(4 8 12 16 20 24 28 32 36 40 44 48 52 56 60 64 68 72 76 80))
               (setq php-manual-path "/usr/local/share/php/doc/html")
               (setq php-manual-url "http://www.phppro.jp/phpmanual/")))
  )

;; markdown ------------------------------------------------------
(when (locate-library "markdown-mode")
  (autoload 'markdown-mode "markdown-mode"
    "Major mode for editing Markdown files" t)
  (setq auto-mode-alist
        (cons '("\\.text" . markdown-mode) auto-mode-alist)))

;; scala-mode -----------------------------------------------
(when (locate-library "scala-mode")
  (require 'scala-mode-auto)
  (require 'scala-mode-feature-electric)
  (add-hook 'scala-mode-hook 'scala-electric-mode)
  (add-hook 'scala-electric-mode-hook '(lambda ()
                                         (autopair-mode -1))))

;; common-lisp mode -----------------------------------------
(when (locate-library "slime")
  (if darwin-p (setq inferior-lisp-program "/usr/local/bin/sbcl"))
  (if linux-p (setq inferior-lisp-program "sbcl")) ;; "ccl64"))
  (require 'slime-autoloads)
  (setq slime-net-coding-system 'utf-8-unix)
  (submodule 'rosemacs
   (invoke-rosemacs)
   (global-set-key "\C-x\C-r" ros-keymap)
   (slime-setup '(slime-fancy slime-asdf slime-ros slime-repl slime-banner slime-indentation slime-scratch)))
  (slime-setup '(slime-fancy slime-asdf slime-repl slime-banner slime-indentation slime-scratch)))


(when (locate-library "popwin")
  (require 'popwin)
  (popwin-mode 1)
  ;; Apropos
  (push '("*slime-apropos*") popwin:special-display-config)
  ;; Macroexpand
  (push '("*slime-macroexpansion*") popwin:special-display-config)
  ;; Help
  (push '("*slime-description*") popwin:special-display-config)
  ;; Compilation
  (push '("*slime-compilation*" :noselect t) popwin:special-display-config)
  ;; Cross-reference
  (push '("*slime-xref*") popwin:special-display-config)
  ;; Debugger
  (push '(sldb-mode :stick t) popwin:special-display-config)
  ;; REPL
  (push '(slime-repl-mode) popwin:special-display-config)
  ;; Connections
  (push '(slime-connection-list-mode) popwin:special-display-config))

;; tex-mode ---------------------------------------------
(when (locate-library "yatex")
  (setq auto-mode-alist (cons (cons "\\.tex$" 'yatex-mode) auto-mode-alist))
  (autoload 'yatex-mode "yatex" "Yet Another LaTeX mode" t)
  (setq auto-mode-alist (append
                         '(("\\.tex$" . yatex-mode)
                           ("\\.ltx$" . yatex-mode)
                           ("\\.cls$" . yatex-mode)
                           ("\\.sty$" . yatex-mode)
                           ("\\.clo$" . yatex-mode)
                           ("\\.bbl$" . yatex-mode)) auto-mode-alist))
  (setq YaTeX-fill-column nil)
  )

;; python mode ---------------------------------------------
(submodule 'jedi
  (add-hook 'python-mode-hook 'jedi:setup)
  (add-hook 'python-mode-hook 'jedi:ac-setup)
;;  (if darwin-p
;;      (push "~/.emacs.d/.python-environments/default/bin/jediepcserver.py" jedi:server-command))
  (setq jedi:complete-on-dot t)
  )

;; euslisp ----------------------------------------------
;; to change indent for euslisp's method definition ;; begin
(define-derived-mode euslisp-mode lisp-mode
  "EusLisp"
  "Major Mode for EusLisp"
  )
(defun lisp-indent-function (indent-point state)
  "This function is the normal value of the variable `lisp-indent-function'.
It is used when indenting a line within a function call, to see if the
called function says anything special about how to indent the line.

INDENT-POINT is the position where the user typed TAB, or equivalent.
Point is located at the point to indent under (for default indentation);
STATE is the `parse-partial-sexp' state for that position.

If the current line is in a call to a Lisp function
which has a non-nil property `lisp-indent-function',
that specifies how to do the indentation.  The property value can be
* `defun', meaning indent `defun'-style;
* an integer N, meaning indent the first N arguments specially
  like ordinary function arguments and then indent any further
  arguments like a body;
* a function to call just as this function was called.
  If that function returns nil, that means it doesn't specify
  the indentation.

This function also returns nil meaning don't specify the indentation."
  (let ((normal-indent (current-column)))
    (goto-char (1+ (elt state 1)))
    (parse-partial-sexp (point) calculate-lisp-indent-last-sexp 0 t)
    (if (and (elt state 2)
             (not (looking-at "\\sw\\|\\s_")))
        ;; car of form doesn't seem to be a symbol
        (progn
          (if (not (> (save-excursion (forward-line 1) (point))
                      calculate-lisp-indent-last-sexp))
              (progn (goto-char calculate-lisp-indent-last-sexp)
                     (beginning-of-line)
                     (parse-partial-sexp (point)
                                         calculate-lisp-indent-last-sexp 0 t)))
          ;; Indent under the list or under the first sexp on the same
          ;; line as calculate-lisp-indent-last-sexp.  Note that first
          ;; thing on that line has to be complete sexp since we are
          ;; inside the innermost containing sexp.
          (backward-prefix-chars)
          (current-column))
      (let ((function (buffer-substring (point)
                                        (progn (forward-sexp 1) (point))))
            method)
        (setq method (or (get (intern-soft function) 'lisp-indent-function)
                         (get (intern-soft function) 'lisp-indent-hook)))
        (cond ((or (eq method 'defun)
                   (and
                    (eq major-mode 'euslisp-mode)
                    (string-match ":.*" function))
                   (and (null method)
                        (> (length function) 3)
                        (string-match "\\`def" function)))
               (lisp-indent-defform state indent-point))
              ((integerp method)
               (lisp-indent-specform method state
                                     indent-point normal-indent))
              (method
               (funcall method indent-point state)))))))
;; to change indent for euslisp's method definition ;; end

;; arduino-mode -----------------------------------------
(submodule 'arduino-mode
  (setq auto-mode-alist (cons '("\\.\\(pde\\|ino\\)$" . arduino-mode) auto-mode-alist))
  (autoload 'arduino-mode "arduino-mode" "Arduino editing mode" t))

;; Go ----------------------------------------------------
(submodule 'go-mode-load)

(eval-after-load "go-mode"
  '(progn
     (submodule 'go-autocomplete)
     (submodule 'auto-complete-config)))
(submodule 'go-eldoc
  (add-hook 'go-mode-hook 'go-eldoc-setup))
(submodule 'golint)

;; web
(submodule 'web-mode
 (when (< emacs-major-version 24)
   (defalias 'prog-mode 'fundamental-mode))

 (add-to-list 'auto-mode-alist '("\\.phtml$"     . web-mode))
 (add-to-list 'auto-mode-alist '("\\.tpl\\.php$" . web-mode))
 (add-to-list 'auto-mode-alist '("\\.jsp$"       . web-mode))
 (add-to-list 'auto-mode-alist '("\\.as[cp]x$"   . web-mode))
 (add-to-list 'auto-mode-alist '("\\.erb$"       . web-mode))
 (add-to-list 'auto-mode-alist '("\\.html?$"     . web-mode))
 (add-to-list 'auto-mode-alist '("\\.ejs$"       . web-mode))

 (set-face-attribute 'web-mode-html-tag-bracket-face nil :foreground "blue")

 (defun web-mode-hook-func ()
   "Hooks for Web mode."
   (setq web-mode-markup-indent-offset 2)
   (setq web-mode-css-indent-offset    2)
   (setq web-mode-code-offset 2))
 (add-hook 'web-mode-hook 'web-mode-hook-func))

;; nginx ------------------------------------------------------
(submodule 'nginx-mode
  (add-to-list 'auto-mode-alist '("/etc/nginx/sites-available/.*" . nginx-mode)))

;; clojure ----------------------------------------------------
(when (locate-library "cider-mode")
  (add-hook 'clojure-mode-hook 'cider-mode)
  (add-hook 'cider-mode-hook 'cider-turn-on-eldoc-mode)
  (setq nrepl-hide-special-buffers t)
  (setq nrepl-buffer-name-show-part t)
  (submodule 'ac-nrepl
             (add-hook 'cider-repl-mode-hook 'ac-nrepl-setup)
             (add-hook 'cider-mode-hook 'ac-nrepl-setup)
             (eval-after-load "auto-complete"
               '(add-to-list 'ac-modes 'cider-repl-mode))))

(submodule 'kibit
 ;; Teach compile the syntax of the kibit output
(autoload 'compile "compile" nil t)
(add-to-list 'compilation-error-regexp-alist-alist
             '(kibit "At \\([^:]+\\):\\([[:digit:]]+\\):" 1 2 nil 0))
(add-to-list 'compilation-error-regexp-alist 'kibit)

;; A convenient command to run "lein kibit" in the project to which
;; the current emacs buffer belongs to.
(defun kibit ()
  "Run kibit on the current project.
Display the results in a hyperlinked *compilation* buffer."
  (interactive)
  (compile "lein kibit"))

(defun kibit-current-file ()
  "Run kibit on the current file.
Display the results in a hyperlinked *compilation* buffer."
  (interactive)
  (compile (concat "lein kibit " buffer-file-name))))


(provide 'init-major-mode)
