echo "export DOTFILES_ROOT=\"$DOTFILES_ROOT\"" >> $HOME/.bashrc
echo 'source $DOTFILES_ROOT/bash/bashrc.common' >> $HOME/.bashrc

echo "installing hub..."
HUB_VERSION=2.2.1
if [ ! -d "$HOME/.hub" ]; then
    mkdir $HOME/.hub
    cd $HOME/.hub
    wget "https://github.com/github/hub/releases/download/v${HUB_VERSION}/hub-linux-amd64-${HUB_VERSION}.tar.gz" -O hub.tar.gz
    tar zxf hub.tar.gz
    \rm hub.tar.gz
    echo 'export PATH=$HOME/.hub/'"hub_${HUB_VERSION}_linux_amd64"':$PATH' >> $HOME/.bashrc.custom
    echo 'source $HOME/.hub/'"hub_${HUB_VERSION}_linux_amd64"'/etc/hub.bash_completion.sh' >> $HOME/.bashrc.custom
fi

if  [ "$(echo $SHELL | grep 'bash')" = "" ]; then
    echo "chsh to bash..."
    chsh -s /bin/bash
fi
