if has('gui_macvim')
    set transparency=10	" 透明度を指定
    set antialias
"    set guifont=Monaco:h13
    set guifont=SourceCodePro-Regular:h12
    set imdisable
    set lines=50 columns=100
    colorscheme evening
    set showtabline=2
    set guioptions+=a
    inoremap ¥ \ 
    inoremap \ ¥
endif
