#!/bin/bash

set -e

if [ `which brew` = "" ]; then
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

brew update
brew upgrade

brew tap homebrew/versions || true
brew tap homebrew/binary || true
brew tap homebrew/dupes || true

brew install caskroom/cask/brew-cask
brew install zsh
brew install zsh-completions
brew install zsh-lovers
brew install zsh-syntax-highlighting
brew install git
brew install ctags
brew install tmux
brew install tig
brew install wget
brew install curl
brew install jq
brew install pt
brew install boot2docker
brew install ffmpeg
brew install gibo
brew install nginx
brew install ifstat
brew install imagemagick
brew install iperf
brew install mercurial
brew install subversion
brew install mysql
brew install mongodb
brew install nmap
brew install pkg-config
brew install typesafe-activator
brew install python
brew install rlwrap
brew install sbcl
brew install scala
brew install ssh-copy-id
brew install tree
brew install ghc cabal-install
brew install go
brew install bash-completion
brew install binutils
brew install coreutils

brew cask install dash
brew cask install iterm2
brew cask install google-chrome
brew cask install firefox
brew cask install kobito
brew cask install dropbox
brew cask install lastpass-universal
brew cask install the-unarchiver
brew cask install bartender
brew cask install skype
brew cask install xquartz
brew cask install bitcasa
brew cask install flash-player
brew cask install flip4mac
brew cask install intellij-idea-ce
brew cask install java
brew cask install macwinzipper
brew cask install mailbox
brew cask install mamp
brew cask install vmware-fusion
brew cask install wireshark
brew cask install emacs
brew cask install transmission
brew cask install teamviewer
brew cask install opera
brew cask install macvim
brew cask install handbrake
brew cask install atom
brew cask install arduino
brew cask install appcleaner
brew cask install eclipse-platform
brew cask install knock
brew cask install macs-fan-control
brew cask install sourcetree
brew cask install vlc
brew cask install hub
