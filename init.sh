#!/bin/bash

yes-or-no() {
    local Answer
    while true; do
        read -p "$@ [Y/n]" Answer
        case $Answer in
            [Yy]* )
                (echo "Yes")
                return 0
                break
                ;;
            '' | [Nn]* )
                (echo "No")
                return 1
                break
                ;;
            * )
                echo Please answer Yes or No.
        esac
    done
}

export DOTFILES_ROOT=`pwd`

if yes-or-no "shell settings?"; then
    if yes-or-no "Do you like zsh?"; then
        if ! which zsh; then
            echo "zsh is not installed."
            exit 1
        fi
        echo "initializing zsh settings..."
        (zsh zsh/init-zsh.zsh)
    else
        echo "initializing bash settings..."
        if [ -e $HOME/.bashrc ]; then
            (bash $DOTFILES_ROOT/bash/init-bash.bash)
        else
            echo "cannot find .bashrc. Skipping..."
        fi
    fi
    if yes-or-no "Do you like percol?"; then
        if [ `uname` = "Darwin" ]; then
            pip install percol
        elif [ `uname` = "Linux" ]; then
            if ! which pip; then
                sudo apt-get install python-pip
            fi
            sudo pip install percol
        fi
    fi
fi

if yes-or-no "ssh keys/configs?"; then
    if [ -e $HOME/.ssh ]; then
        mv ~/.ssh ~/.ssh.old
    fi
    ln -s $DOTFILES_ROOT/ssh ~/.ssh
    chmod 0700 ~/.ssh
    chmod 0600 ~/.ssh/*id_rsa*
fi

if yes-or-no "emacs settings?"; then
    if yes-or-no "install emacs24?"; then
	echo "Install emacs24..."
	if [ "`uname`" = "Linux" ]; then
	    if [ "`lsb_release -rs`" = "12.04" ]; then
	        sudo add-apt-repository ppa:cassou/emacs
	        sudo apt-get update
	    fi
	    sudo apt-get install emacs24-nox
        elif [ "`uname`" = "Darwin" ]; then
            brew install emacs
        else
            echo "Please install emacs24 manually."
        fi
    fi

    if [ -e $HOME/.emacs.d ]; then
        sudo mv ~/.emacs.d ~/.emacs.d.old
    fi
    ln -s $DOTFILES_ROOT/emacs.d ~/.emacs.d

    if [ -d $HOME/.cask ]; then
	sudo rm -rf $HOME/.cask
    fi
    echo "install emacs cask"
    if ! which curl; then
	sudo apt-get install curl
    fi
    curl -fsSL https://raw.githubusercontent.com/cask/cask/master/go | python
    export PATH=$HOME/.cask/bin:$PATH
    (cd $HOME/.emacs.d && cask install)
fi


if yes-or-no "git config?"; then
    if [ -e $HOME/.gitignore ]; then
        mv $HOME/.gitignore .gitignore.old
    fi
    if echo `git --version` | grep 1.7.; then
        ln -s $DOTFILES_ROOT/gitconfig-1.7 ~/.gitconfig
    else
        ln -s $DOTFILES_ROOT/gitconfig-1.8 ~/.gitconfig
    fi
fi

echo "reloading bash..."
exec -l $SHELL
