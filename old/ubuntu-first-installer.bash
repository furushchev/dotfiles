# add repository

for OPT in "$@"
do
    case "$OPT" in
        '-f' | '--font' )
            INS_FONT=1
            break
    esac
done

function add_(){
    sudo add-apt-repository -y $1
}

add_ ppa:ikoinoba/ppa
#add_ ppa:cassou/emacs
add_ ppa:tualatrix/ppa
add_ ppa:webupd8team/java
add_ ppa:byobu/ppa
add_ ppa:ozcanesen/terra-terminal
add_ ppa:webupd8team/y-ppa-manager
add_ ppa:relan/exfat
add_ ppa:mitya57
add_ ppa:caffeine-developers/ppa

# update
sudo apt-get install -yV aptitude
sudo aptitude update && sudo aptitude -y upgrade

function ins_(){
    sudo aptitude install -yV $@
}

 # essential
ins_ build-essential git-all python2.7-dev automake wget curl
ins_ python-setuptools python-pip python-software-properties
#ins_ emacs24 emacs24-el emacs24-common-non-dfsg
ins_ ibus-mozc
ins_ vim-gnome
ins_ terra byobu screen tmux
ins_ tree htop
ins_ imagemagick
ins_ meld
ins_ y-ppa-manager
ins_ fuse-exfat
ins_ xsel

 # web
ins_ nginx apache2
ins_ php5 php5-cgi php5-cli php5-mysql php5-gd php-apc php5-fpm
ins_ mysql-server
ins_ retext
ins_ scala

 # other desktop applications
ins_ evolution
ins_ indicator-multiload
ins_ gimp vlc
ins_ nautilus-image-converter nautilus-open-terminal
#ins_ hotot

 # gnome3
#ins_ gnome-shell
#ins_ gnome-common libglib2.0-dev
ins_ ubuntu-tweak gnome-tweak-tool

# others
ins_ ubuntu-restricted-extras
ins_ oracle-java7-installer
ins_ caffeine python-glade2

# remove needless packages
sudo aptitude remove --purge unity-lens-shopping unity-webapps-common

# change directory name to english
LANG=C xdg-user-dirs-gtk-update

# install chrome
#wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
#sudo echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list
#sudo chmod 644 /etc/apt/sources.list.d/google-chrome.list
#sudo aptitude update
#sudo aptitude install -yV google-chrome-stable

# setup dotfiles
mkdir ~/Development
mv ~/.bashrc ~/.bashrc.default
ln -s ~/Development/dotfiles/bashrc.common ~/.bashrc
ln -s ~/Development/dotfiles/emacs.d ~/.emacs.d
ln -s ~/Development/dotfiles/vimrc ~/.vimrc
ln -s ~/Development/dotfiles/gvimrc ~/.gvimrc
ln -s ~/Development/dotfiles/.vim ~/.vim
touch ~/.bashrc.custom
source ~/.bashrc

# setup play framework
#cd ~/Development
#curl -O http://downloads.typesafe.com/play/2.1.0/play-2.1.0.zip
#unzip play-2.1.0.zip
#echo export PATH=~/Development/play-2.1.0:$PATH >> ~/.bashrc.custom
#source ~/.bashrc

# ipad_charge
ins_ libusb-1.0-0 libusb-1.0-0-dev
git clone https://github.com/mkorenkov/ipad_charge.git ~/Development/ipad_charge
(cd ~/Development/ipad_charge && make && sudo make install)

# silverlight_search
ins_ automake pkg-config libpcre3-dev zlib1g-dev liblzma-dev
git clone https://github.com/ggreer/the_silver_searcher.git ~/Development/the_silver_searcher
(cd ~/Development/the_silver_searcher && ./build.sh && sudo make install)

if [ -n ${INS_FONT} ]; then
    ins_ fontforge
    mkdir /tmp/ricty
    wget 'https://github.com/yascentur/Ricty/archive/3.2.2.zip' -O /tmp/ricty.zip
    (cd /tmp && unzip -j -d ricty ricty.zip)
    wget -O /tmp/migu-1m.zip 'http://sourceforge.jp/frs/redir.php?m=iij&f=%2Fmix-mplus-ipa%2F59022%2Fmigu-1m-20130617.zip'
    (cd /tmp && unzip -j -d ricty migu-1m.zip)
    wget 'http://levien.com/type/myfonts/Inconsolata.otf' -O /tmp/ricty/Inconsolata.otf
    mkdir ~/.fonts
    (cd /tmp/ricty && sh ricty_generator.sh auto && sudo cp -f Ricty*.ttf ~/.fonts)
    fc-cache -vf
    ins_ gconf-editor
    gconf-editor
fi
