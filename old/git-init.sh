#!/bin/bash

function g_(){
    git config --global alias.$1 $2
}

git config --global alias.a  add
git config --global alias.st status
git config --global alias.cl clone
git config --global alias.co checkout
git config --global alias.ci commit
git config --global alias.st 'status -sb'
git config --global alias.br 'branch -a'
git config --global alias.re 'remote -v'
git config --global alias.ps push
git config --global alias.pl pull
git config --global alias.l1 "log --all --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --date=relative"


git config --global help.autocorrect 1
git config --global color.ui 1