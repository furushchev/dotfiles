function translate(){
    local FROM
    local TO
    if [ $# -eq 1 ]; then
        FROM="en"
        TO="ja"
    elif [ $# -eq 3 ]; then
        FROM=$2
        TO=$3
    else
        echo "[usage] translate phrase [from(en) to(ja)]" 1>&2
    fi
    if [ "$TO" != "" ]; then
        echo "$1 ($FROM->$TO)"
        curl -sSX GET "http://glosbe.com/gapi/translate?from=$FROM&dest=$TO&format=json&phrase=$1&pretty=true" | jq -r '[.tuc[]|if .phrase == null then empty else {w:.phrase.text, m:.meanings} end]|reduce .[] as $i ("\n"; . + $i.w + "\n" + reduce $i.m[] as $m (""; . + "\t" + $m.text + "\n"))' 2>/dev/null
    fi
}

function transexpl(){
    local FROM
    local TO
    if [ $# -eq 1 ]; then
        FROM="en"
        TO="ja"
    elif [ $# -eq 3 ]; then
        FROM=$2
        TO=$3
    else
        echo "[usage] transexpl phrase [from(en) to(ja)]" 1>&2
    fi
    if [ "$TO" != "" ]; then
        echo "$1 ($FROM->$TO)"
        curl -sSX GET "http://glosbe.com/gapi/tm?from=$FROM&dest=$TO&format=json&phrase=$1&pretty=true" | jq -r --arg FROM "$FROM" --arg TO "$TO" 'reduce .examples[] as $e ("";. + $FROM+":\t" + $e.first + "\n"+$TO+":\t" + $e.second + "\n\n")' 2>/dev/null|less
    fi
}
