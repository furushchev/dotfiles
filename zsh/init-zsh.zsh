export ZSH_ROOT=`pwd`
export ZDOTDIR=$HOME/.zsh

echo "backup existing zsh settings..."
RCFILES=(.zlogin .zlogout .zprofile .zpreztorc .zshrc .zsh)
(cd $HOME && mkdir zsh_backup && mv $RCFILES zsh_backup)

echo "symlink $ZSH_ROOT/zsh/ -> $ZDOTDIR"
ln -s $ZSH_ROOT/zsh/ $ZDOTDIR

echo "bootstrap .zshenv"
if [ ! -e $HOME/.zshenv ]; then
    touch $HOME/.zshenv
fi
echo "export DOTFILES_ROOT=\"$DOTFILES_ROOT\"" >> $HOME/.zshenv
echo 'ZDOTDIR=$HOME/.zsh' >> $HOME/.zshenv
echo 'source $ZDOTDIR/.zshenv' >> $HOME/.zshenv


if [ `uname` = "Linux" ]; then
    echo "downloading font..."
    if [ ! -d "$HOME/.fonts" ]; then
        mkdir $HOME/.fonts
    fi
    if ! which wget; then
        sudo apt-get install wget
    fi
    if [ ! -e $HOME/.fonts/Symbola.ttf ]; then
        (cd $HOME/.fonts && \
            wget http://download.damieng.com/fonts/redistributed/DroidFamily.zip && \
            unzip DroidFamily.zip && \rm DroidFamily.zip && \
            wget http://users.teilar.gr/~g1951d/Symbola.zip && \
            unzip Symbola.zip && \rm Symbola.zip)
    fi
fi

echo "initializing prezto..."
if [ -d "${ZDOTDIR:-$HOME}/.zprezto" ]; then
    (cd "${ZDOTDIR:-$HOME}/.zprezto" && git pull && git submodule update --init --recursive)
else
    git clone --recursive https://github.com/sorin-ionescu/prezto.git "${ZDOTDIR:-$HOME}/.zprezto"
fi

echo "installing hub..."
HUB_VERSION=2.2.1
if [ ! -d "${ZDOTDIR}/.hub" ]; then
    mkdir ${ZDOTDIR}/.hub
    cd ${ZDOTDIR}/.hub
    wget "https://github.com/github/hub/releases/download/v${HUB_VERSION}/hub-linux-amd64-${HUB_VERSION}.tar.gz" -O hub.tar.gz
    tar zxf hub.tar.gz
    \rm hub.tar.gz
    echo 'export PATH=${ZDOTDIR}/.hub/'"hub_${HUB_VERSION}_linux_amd64"':$PATH' >> $HOME/.zshrc.custom
    echo 'source ${ZDOTDIR}/.hub/'"hub_${HUB_VERSION}_linux_amd64"'/etc/hub.zsh_completion' >> $HOME/.zshrc.custom
fi

if [ "$(echo $SHELL | grep 'zsh')" = "" ]; then
    echo "chsh"
    chsh -s /bin/zsh
fi
